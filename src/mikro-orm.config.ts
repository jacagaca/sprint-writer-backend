import { __prod__ } from "./consts";
import { Post } from "./entities/Post";
import { MikroORM } from "mikro-orm";
import path from "path";
import { User } from "./entities/User";

export default {
  migrations: {
    path: path.join(__dirname, "./migrations"), // path to the folder with migrations
    pattern: /^[\w-]+\d+\.[tj]s$/, // regex pattern for t
  },
  dbName: "SprintWriterDB",
  user: "postgres",
  password: "ehz6qz73",
  debug: !__prod__,
  type: "postgresql",
  entities: [Post, User],
} as Parameters<typeof MikroORM.init>[0];
