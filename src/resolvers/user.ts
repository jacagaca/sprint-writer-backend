import {
  Resolver,
  Query,
  Ctx,
  Arg,
  Mutation,
  InputType,
  Field,
  ObjectType,
} from "type-graphql";
import { User } from "../entities/User";
import { MyContext } from "src/types";
import argon2 from "argon2";
import { EntityManager } from "mikro-orm";
import { COOKIE_NAME } from "../consts";
import { sendEmail } from "../utils/sendEmail";
import { v4 } from "uuid";
//for arguments
@InputType()
class UsernamePasswordEmailInput {
  @Field()
  username: string;
  @Field()
  password: string;
  @Field()
  email: string;
}

@InputType()
class EmailPasswordInput {
  @Field()
  email: string;
  @Field()
  password: string;
}

@ObjectType()
class FieldError {
  @Field()
  field: string;
  @Field()
  message: string;
}

//for returns
@ObjectType()
class UserResponse {
  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];

  @Field(() => User, { nullable: true })
  user?: User;
}

@InputType()
class EmailInput {
  @Field()
  email: string;
}

@Resolver()
export class UserResolver {
  @Query(() => User, { nullable: true })
  async me(@Ctx() { em, req }: MyContext) {
    if (!req.session!.userId) return null;

    const user = await em.findOne(User, { id: req.session!.userId });
    return user;
  }

  @Mutation(() => UserResponse)
  async registerUser(
    @Arg("options") options: UsernamePasswordEmailInput,
    @Ctx() { em, req }: MyContext
  ): Promise<UserResponse> {
    const hashedPassword = await argon2.hash(options.password);

    if (options.username.length <= 2 || options.username.length > 25) {
      return {
        errors: [
          {
            field: "username",
            message: "Username filed length must have from 3 to 25 characters.",
          },
        ],
      };
    }

    if (options.email.length <= 1 || options.email.length > 50) {
      return {
        errors: [
          {
            field: "email",
            message: "Email field length must have from 2 to 50 characters.",
          },
        ],
      };
    }

    if (options.password.length <= 2 || options.password.length > 25) {
      return {
        errors: [
          {
            field: "password",
            message: "Password field length must have from 3 to 25 characters.",
          },
        ],
      };
    }

    let user;
    try {
      const result = await (em as EntityManager)
        .createQueryBuilder(User)
        .getKnexQuery()
        .insert({
          username: options.username,
          password: hashedPassword,
          email: options.email,
          created_at: new Date(),
          updated_at: new Date(),
        })
        .returning("*");
      user = result[0];
    } catch (err) {
      if (err.code === "23505" && err.detail.includes("username")) {
        return {
          errors: [
            {
              field: "username",
              message: "This username is already taken.",
            },
          ],
        };
      } else if (err.code === "23505" && err.detail.includes("email")) {
        return {
          errors: [
            {
              field: "email",
              message: "This email address is already taken.",
            },
          ],
        };
      }
    }

    req.session!.userId = user.id;

    return {
      user,
    };
  }

  @Mutation(() => UserResponse)
  async loginUser(
    @Arg("options") options: EmailPasswordInput,
    @Ctx() { em, req }: MyContext
  ): Promise<UserResponse> {
    const user = await em.findOne(User, {
      email: options.email,
    });

    if (!user) {
      return {
        errors: [
          { field: "email", message: "User with that name doesn't exists." },
        ],
      };
    }

    req.session!.userId = user.id;

    const valid = await argon2.verify(user.password, options.password);
    if (!valid) {
      return {
        errors: [{ field: "password", message: "Wrong password." }],
      };
    }

    return {
      user,
    };
  }

  @Mutation(() => String)
  async logoutUser(@Ctx() { req, res }: MyContext): Promise<String> {
    if (!req.session!.userId) {
      return "You are not logged in";
    }

    await new Promise((resolve) =>
      req.session?.destroy((err) => {
        res.clearCookie(COOKIE_NAME);
        if (err) {
          console.log(err);
          resolve(false);
          return "Error";
        }

        resolve(true);
        return "You successfully logged out";
      })
    );
    return "";
  }

  @Mutation(() => Boolean)
  async forgotPassword(
    @Arg("email") email: string,
    @Ctx() { em, req, res, redisClient }: MyContext
  ) {
    const user = await em.findOne(User, {
      email: email,
    });

    if (email.length <= 3 || email.length > 50) {
      return false;
    }

    if (!user) {
      return true;
    }

    const token = v4();
    const text = `<a href="http://localhost:3000/change-password/${token}">RESET PASSWORD</a>`;

    await redisClient.set(
      `forget password: ${token}`,
      user.id,
      "ex",
      1000 * 60 * 60 * 24
    );

    await sendEmail(email, text);
    return true;
  }

  @Mutation(() => UserResponse)
  async changePassword(
    @Arg("token") token: string,
    @Arg("newPassword") newPassword: string,
    @Ctx() { em, req, res, redisClient }: MyContext
  ): Promise<UserResponse> {
    if (newPassword.length <= 2 || newPassword.length > 25) {
      return {
        errors: [
          {
            field: "newPassword",
            message: "Password field length must have from 3 to 25 characters.",
          },
        ],
      };
    }
    const key = `forget password: ${token}`;
    const userId = await redisClient.get(key);

    if (!userId) {
      return {
        errors: [
          {
            field: "newPassword",
            message: "Token is invalid or expired",
          },
        ],
      };
    }

    const user = await em.findOne(User, {
      id: userId,
    });

    if (!user) {
      return {
        errors: [
          {
            field: "newPassword",
            message: "User no longer exists",
          },
        ],
      };
    }
    const hashedPassword = await argon2.hash(newPassword);

    user.password = hashedPassword;
    await em.persistAndFlush(user);

    if (req.session) {
      req.session.userId = user.id;
    }

    redisClient.del(key);
    return {
      user,
    };
  }
}
