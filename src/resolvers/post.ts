import {
  Ctx,
  Field,
  Arg,
  Mutation,
  Query,
  Resolver,
  InputType,
  ObjectType,
} from "type-graphql";
import { EntityManager } from "mikro-orm";
import { Post } from "../entities/Post";

import { MyContext } from "../types";

@InputType()
class PostInput {
  @Field()
  title: string;
  @Field()
  text: string;
}

@ObjectType()
@Resolver()
export class PostResolver {
  ///////////////////////////////
  @Query(() => [Post], { nullable: true })
  async posts(@Ctx() { em, req }: MyContext) {
    const posts = await em.find(Post, {});

    return posts;
  }

  @Mutation(() => Post)
  async createPost(
    @Arg("options") options: PostInput,
    @Ctx() { em, req }: MyContext
  ): Promise<Post> {
    let post;
    try {
      const result = await (em as EntityManager)
        .createQueryBuilder(Post)
        .getKnexQuery()
        .insert({
          title: options.title,
          text: options.text,
          created_at: new Date(),
          updated_at: new Date(),
        })
        .returning("*");
      post = result[0];
    } catch (err) {
      console.log(err);
    }
    return post;
  }

  @Mutation(() => Boolean)
  async deletePost(@Ctx() { req }: MyContext): Promise<boolean> {
    // not cascade way
    const post = true;
    if (!post) {
      return false;
    }

    return true;
  }
}
